import java.util.Random;
import java.util.logging.Logger;

public class DevEducationException {

    DevEducationException(String message) { super(message); }
}
