package src.com.github.string;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Выберите строку:" + "\n" + "1 - 'A - Z' " + "\n" + "2 - 'z - a'" +
                "\n" + "3 - '0 - 9'" + "\n" + "4 - 'а - я'" +
                "\n" + "5 - 'ASCII'" + "\n");

        Scanner scana = new Scanner(System.in);
        int a = scana.nextInt();

        if(a == 1){

            AZ.az();

        }

        if(a == 2){

            za.za();

        }

        if(a == 3){

            nullnine.nn();

        }

        if(a == 4){

            kyrill.klim();

        }

        if(a == 5){

            ASCII.ascii();

        }

    }
}
