package src.com.github.task;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        System.out.println("Выберите задачу которую хотите решить:" + "\n" + "1 - Задача о стрельбе из гаубицы" + "\n" + "2 - Задача о двух автомобилях" +
                "\n" + "3 - Логическое выражение при точке внутри заштрихованойобласти" + "\n" + "4 - Вычисление значения выражения" +
                "\n" + "Для выбора введите номер задачи");

        Scanner scana = new Scanner(System.in);
        int a = scana.nextInt();

        if(a == 1){

            cannon.cannontask();

        }

        if(a == 2){

            automobile.automobiletask();

        }

        if(a == 3){

            xyspace.xytask();

        }

        if(a == 4){

            logarifm.lntask();

        }

    }
}
