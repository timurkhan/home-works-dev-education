package src.com.github.task;

import java.util.Scanner;

public class cannon {

    public static void cannontask(){

        System.out.println("Если вы хотите реализовать задачу через градусы введите 1 , если через радианы введите 2");
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        double g = 9.81;

        if (x == 1){
            System.out.println("Введите угол возвышение ствола а = ");
            double a = scanner.nextDouble();

            System.out.println("Введите начальную скорость v = ");
            double v = scanner.nextDouble();

            double sin = Math.sin(2 * a);

            double S = ((v * v) * sin) / g;

            System.out.println("Расстояние на которое полетел снаряд S = " + S + "км");

        }
        else if (x == 2){
            System.out.println("Введите угол возвышение ствола а = ");
            double a = scanner.nextDouble();
            double b = (a * 180)/Math.PI;

            System.out.println("Введите начальную скорость v = ");
            double v = scanner.nextDouble();

            double sin = Math.sin(2 * b);

            double S = ((v * v) * sin) / g;

            System.out.println("Расстояние на которое полетел снаряд S = " + S + "км");

        }
        else if (x != 1 && x != 2){
            System.out.println("Следаволо ввести 1 или 2");
        }


    }
}
