package src.com.github.random;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Выберите генератор случайных чисел:" + "\n" + "1 - случайное число" + "\n" + "2 - 10 случайных чисел" +
                "\n" + "3 - 10 случайных чисел, каждое в диапазоне от 0 до 10" + "\n" + "4 - 10 случайных чисел, каждое в диапазоне от 20 до 50" +
                "\n" + "5 - 10 случайных чисел, каждое в диапазоне от -10 до 10" + "\n" + "6 - случайное количество (в диапазоне от 3 до 15) " +
                "случайных чисел, каждое в диапазоне от -10 до 35" + "\n" + "Для выбора введите номер генератора случайных чисел");

        Scanner scana = new Scanner(System.in);
        int a = scana.nextInt();

        if(a == 1){

            random.random1();

        }

        if(a == 2){

            random.random2();

        }

        if(a == 3){

            random.random3();

        }

        if(a == 4){

            random.random4();

        }

        if(a == 5){

            random.random5();

        }

        if(a == 6){

            random.random6();

        }
    }
}
