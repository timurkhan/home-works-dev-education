package src.com.github.figures;

public class Figures {

    public static void square() {

        int a = 7;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                System.out.print("*");
            }

            System.out.println();
        }
    }

    public static void emptysquare() {

        int a = 7;
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                if (i == 0 || i == a - 1 || j == 0 || j == a - 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println(" ");
        }
    }

    public static void luptriangle() {

    }

    public static void ldwntriangle() {

    }

    public static void ruptriangle() {

    }

    public static void rdwntriangle() {

    }



    public static void cross() {

        for (int i = 0; i < 7; i++) {
            for (int a = 0; a < 7; a++) {
                if (a == i || a + i == 6) {
                    System.out.print("*");
                } else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }

    public static void uptriangle() {

        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i > array[0].length / 2) {
                    break;
                }
                if (i == 0 || i == (array[1].length - j - 1) || i == j) {
                    System.out.print("*\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }

    }

    public static void dwntriangle() {

        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i < array[0].length/2){
                    break;
                }
                if (i == 6 || i == (array[1].length - j - 1) || i == j){
                    System.out.print("*\t");
                }
                else{
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }

    }
}
