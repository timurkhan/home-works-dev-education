import java.util.Arrays;

import static com.github.oop1.app.DeviceArray.devices;

public class Main {
    public static void main(String[] args) {

        Memory memory1 = new Memory(1024);
        System.out.println(memory1.getMemoryInfo().toString());

        Filter filter = new Filter();
        System.out.println(Arrays.toString(filter.filterAmr(devices)));
        System.out.println(Arrays.toString(filter.filterX86(devices)));
        System.out.println(Arrays.toString(filter.filterFrequency(devices, 2.9f)));
        System.out.println(Arrays.toString(filter.filterMemoryMore(devices, 15000)));
    }
}
