public class Memory {

    private String[] memoryCell;

    private int memorySize;

    public Memory(int memorySize) {

        this.memoryCell = new String[memorySize];
        this.memorySize = memorySize;

    }

    public int getMemorySize() {
        return memorySize;
    }

    public void setMemoryCell(String[] memoryCell) {
        this.memoryCell = memoryCell;
    }

    public String readLast() {

        String str = "";
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] != null) {
                str = memoryCell[i];
                break;
            }
        }
        return str;

    }

    public String removeLast() {

        String str = "";
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] != null) {
                str = memoryCell[i];
                memoryCell[i] = null;
                break;
            }
        }
        return str;
    }

    public boolean save(String str) {

        boolean b = false;
        for (int i = 0; i < memoryCell.length; i++) {
            if (memoryCell[i] == null) {
                memoryCell[i] = str;
                b = true;

            } else {
                b = false;
            }
        }
        return b;
    }

    public MemoryInfo getMemoryInfo() {

        int count = 0;
        double countPercent = 0.0D;
        for (int i = 0; i < memoryCell.length; i++) {

            if (memoryCell[i] != null) {
                count++;
            }
        }
        countPercent = (double) memoryCell.length / 100 * count;
        return new MemoryInfo(memoryCell.length, countPercent);
    }
}
