public class MemoryInfo {

    private int memorySize;

    private double memorySizePercent;

    public MemoryInfo(int memorySize, double memorySizePercent) {

        this.memorySize = memorySize;
        this.memorySizePercent = memorySizePercent;
    }

    public int getMemorySize() {
        return memorySize;
    }

    public double getMemorySizePercent() {
        return memorySizePercent;
    }

    @Override
    public String toString() {

        return "MemoryInfo{" +
                "memorySize=" + memorySize +
                ", memorySizePercent='" + memorySizePercent + "%" + '\'' +
                '}';
    }
}
